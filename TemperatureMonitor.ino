// Include the ESP8266 WiFi library. (Works a lot like the
// Arduino WiFi library.)
#include <ESP8266WiFi.h>
#include <WifiClient.h>
#include <OneWire.h>

#include <DallasTemperature.h>

#define ONE_WIRE_BUS 2
// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

//////////////////////
// WiFi Definitions //
//////////////////////
const char WiFiSSID[] = "";
const char WiFiPSK[] = "";

const int LED_PIN = 0; 

/////////////////
// Post Timing //
/////////////////
const unsigned long postRate = 60000 * 15; //check in once per 15 minutes
unsigned long lastPost = 0;
const char herokuHost[] = "whispering-brook-51660.herokuapp.com";

void connectWiFi(){
  byte ledStatus = LOW;

  // Set WiFi mode to station (as opposed to AP or AP_STA)
  WiFi.mode(WIFI_STA);

  // WiFI.begin([ssid], [passkey]) initiates a WiFI connection
  // to the stated [ssid], using the [passkey] as a WPA, WPA2,
  // or WEP passphrase.
  WiFi.begin(WiFiSSID, WiFiPSK);

  // Use the WiFi.status() function to check if the ESP8266
  // is connected to a WiFi network.
  while (WiFi.status() != WL_CONNECTED){
    // Blink the LED
    digitalWrite(LED_PIN, ledStatus); // Write LED high/low
    ledStatus = (ledStatus == HIGH) ? LOW : HIGH;

    // Delays allow the ESP8266 to perform critical tasks
    // defined outside of the sketch. These tasks include
    // setting up, and maintaining, a WiFi connection.
    delay(100);
    // Potentially infinite loops are generally dangerous.
    // Add delays -- allowing the processor to perform other
    // tasks -- wherever possible.
    //or use yield();
  }
}

void initHardware(){
  //Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
}

void setup() {
  initHardware();
  connectWiFi();
  digitalWrite(LED_PIN, HIGH);

  sensors.begin(); //ds18b20
}

static float getTempReading(){
  sensors.requestTemperatures();  
  return sensors.getTempCByIndex(0);
}

void sendRequest(String deviceId, float celsiusReading, float fahrenheitReading, WiFiClient client){
  //send http get request
  client.print("GET /logService/?deviceName=");
  client.print(deviceId);
  client.print("&reportC=");
  client.print(celsiusReading);
  client.print("&reportF=");
  client.print(fahrenheitReading);
  client.print("&status=");
  client.print("Check-in%20OK.");
  client.println(" HTTP/1.1");
  client.print("Host: ");
  client.println(herokuHost);
  client.println("Connection: close");
  client.println();

  while(client.available()){
    String line = client.readStringUntil('\r');
  }
}

int postToHeroku(){
  // LED turns on when we enter, it'll go off when we 
  // successfully post.
  float celsiusReading;
  float fahrenheitReading;
  digitalWrite(LED_PIN, HIGH);

  // Do a little work to get a unique-ish name. Append the
  // last two bytes of the MAC (HEX'd) to "Thing-":
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.macAddress(mac);
  //String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
  //               String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
  //macID.toUpperCase();
  String deviceId = "ESP8266-0";

  WiFiClient client;
  const int httpPort = 80;
  
  if (!client.connect(herokuHost, httpPort)) {
    // If we fail to connect, return 0.
    return 0;
  }

  celsiusReading = getTempReading();
  fahrenheitReading = (celsiusReading * (9.0 / 5.0)) + 32;
  sendRequest(deviceId, celsiusReading, fahrenheitReading, client);

  digitalWrite(LED_PIN, LOW);

  return 1; // Return success
}


void loop() {
  //create a post right at the start for debugging and verification purposes
  if (lastPost + postRate <= millis()){
    if (postToHeroku())
      lastPost = millis();
    else
      delay(1000);    
  }
}

